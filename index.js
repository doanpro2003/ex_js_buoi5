//câu1
document.getElementById('chan-le').onclick = function () {
    var evenNum = "";
    var odd = "";
    for (var i = 0; i < 100; i++) {
        if (i % 2 == 0)
            evenNum += " " + i;
        else
            odd += " " + i;
    }
    document.getElementById("result1").innerHTML = `<p>👉Số chẵn: ${evenNum}</p> <p>👉Số lẻ: ${odd}</p>`;
}
//Câu2
document.getElementById('chia-het').onclick = function () {
    var count = 0;
    for (var i = 0; i < 1000; i++) {
        if (i % 3 == 0)
            count++;
    }
    document.getElementById("result2").innerHTML = `👉Số chia hết cho 3 nhỏ hơn 1000: ${count} số`;
}
//Câu3
document.getElementById('so-nguyen-nho-nhat').onclick = function () {
    var sum = 0;
    var number = 0;
    while (sum < 10000) {
        number++;
        sum += number;
    }
    document.getElementById("result3").innerHTML = `👉Số nguyên dương nhỏ nhất: ${number}`;
}
//Câu4
document.getElementById('sum').onclick = function () {
    var sum = 0;
    var x = document.getElementById("txt-num-x").value * 1;
    var n = document.getElementById("txt-num-n").value * 1;
    for (var i = 1; i <= n; i++) {
        sum += x ** i;
    }
    document.getElementById("result4").innerHTML = `👉Tổng: ${sum}`;
}
//Câu5
document.getElementById('giaiThua').onclick = function () {
    var numN = document.getElementById("txt-num-n2").value * 1;
    var result = 1;
    for (var i = 1; i <= numN; i++) {
        result = result * i;
    }
    document.getElementById("result5").innerHTML = `👉Giai thừa: ${result}`;
}
//Câu6
document.getElementById('the-div').onclick = function () {
    var content = "";
    for (var i = 1; i <= 10; i++) {
        if (i % 2 == 0)
            content += `<div class="div-chan p-2" style="background-color:#DC3444;">Div chẵn</div>`;
        else
            content += `<div class="div-le p-2" style="background-color:#0C6EFD;">Div lẻ</div>`;
    }
    document.getElementById("result6").innerHTML = `👉 ${content}`;
}
//Câu7
function demcacuocso(n) {
    var dem = 0;
    for (var i = 1; i <= n; i++) {
        if (n % i == 0) {
            dem++;
        }
    }
    return dem;
}
function ktsnt(n) {
    if (demcacuocso(n) == 2)
        return 1;
    return 0;
}
document.getElementById('nguyen-to').onclick = function () {
    var result = "";
    var n = document.getElementById('txt-num-n3').value * 1;
    for (var i = 1; i <= n; i++) {
        if (ktsnt(i) == 1) {
            result += " " + i;
        }
    }
    document.getElementById("result7").innerHTML = `👉 ${result}`;

}
